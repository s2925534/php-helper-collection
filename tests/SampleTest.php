<?php

declare(strict_types=1);

namespace Tests;

use App\ApplicationTest;
use PHPUnit\Framework\TestCase;

class SampleTest extends TestCase
{
    public function testGetSampleAPIBody()
    {
        $app = new ApplicationTest();
        $event = '{}';
        $expectedResponse = ['status' => 'success', 'message' => 'success', 'data' => null];
        $this->assertEquals($expectedResponse,
            $app->getSampleAPIBody($event));
    }

    public function testGetSampleAPIBodyData()
    {
        $app = new ApplicationTest();
        $event = '{}';
        $expectedResponse = ['status' => 'success', 'message' => 'success', 'data' => null];
        $expectedResponse = $expectedResponse['data'];
        $this->assertEquals($expectedResponse,
            $app->getSampleAPIBodyData($event));
    }
}
