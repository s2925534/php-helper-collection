<?php

declare(strict_types=1);

namespace App;

class ApplicationTest
{
    public function __construct()
    {
    }

    public function getSampleAPIBody($event)
    {
        return ['status' => 'success', 'message' => 'success', 'data' => null];
    }

    public function getSampleAPIBodyData($event)
    {
        return null;
    }
}
