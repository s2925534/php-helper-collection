<?php

namespace PHPHelperCollection\CloudFileStorage\AWS;

use Aws\Exception\AwsException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Base class to wrap up AWS services.
 */
abstract class General implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    const RESPONSE_CODE_OK = 0;
    const RESPONSE_CODE_ERROR_UNKNOWN = -1;
    const RESPONSE_MESSAGE_ERROR_UNHANDLED = 'Unhandled error happened (standard exception).';

    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    abstract protected function handleAWSError($e);

    //Must be implemented in derived classes

    /**
     * Wrap AWS errors for including in response
     * @param $message
     * @param $code
     * @param $exception AWS Exception Object
     */
    protected function handleError($message, $code, $exception = null)
    {
        //Init response
        $response = $this->initResponse();

        //Populate body
        $body = [];
        if (!empty($exception)) {
            if ($exception instanceof AwsException) {
                $body['message'] = $exception->getAwsErrorMessage();
                $body['code'] = $exception->getAwsErrorCode();
            } else {
                $body['message'] = $exception->getMessage();
                $body['code'] = $exception->getCode();
            }
        } else {
            $body['message'] = 'No exception object provided';
            $body['code'] = null;
        }

        //Populate response
        $response['message'] = $message;
        $response['body'] = $body;
        $response['code'] = $code;

        return $response;
    }

    protected function initResponse()
    {
        return [
            'message' => '',
            'body' => [],
            'code' => null,
        ];
    }
}
