<?php

namespace App\Service\AWS;

use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Exception;
use PHPHelperCollection\CloudFileStorage\AWS\General;

/**
 * Wraps the AWS S3Client class & provides specific utility methods
 */
class S3 extends General
{
    const RESPONSE_CODE_ERROR_NO_KEY = -2;
    const RESPONSE_CODE_ERROR_NO_KEY_FOUND = -3;

    protected $client;
    protected $bucketName;

    public function __construct(array $config)
    {
        parent::__construct($config);

        /**
         * Client init.
         * Set AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY as env variables
         */
        $this->client = new S3Client([
            'region' => $this->config['region'],
            'version' => $this->config['version'],
        ]);

        //Init bucket name
        $this->bucketName = $this->config['s3']['bucket_name'];
    }

    public function getBucket()
    {
        return $this->config['s3']['bucket_name'];
    }

    /**
     * Put a raw object of any kind in S3.
     * @param key name/path of the file
     * @param body string or blob to upload
     * @param bucket option bucket override
     */
    public function putObject(string $key, $body, string $bucket = null)
    {
        //Init response
        $response = $this->initResponse();

        //Check if key has been given
        if (!empty($key)) {
            try {
                $s3Object = $this->client->putObject([
                    'Bucket' => !empty($bucket) ? $bucket : $this->bucketName, //Required
                    'Key' => $key, //Required
                    'Body' => $body,
                ]);

                //Populate response
                $response['body'] = [
                    'object' => $s3Object,
                    'data' => $this->generateUrl($key),
                ];
                $response['code'] = static::RESPONSE_CODE_OK;
            } catch (AwsException | S3Exception $exception) {
                $response = $this->handleAWSError($exception);
            } catch (Exception $exception) {
                $response = $this->handleError(
                    static::RESPONSE_MESSAGE_ERROR_UNHANDLED,
                    static::RESPONSE_CODE_ERROR_UNKNOWN,
                    $exception
                );
            }
        } else {
            $response = $this->handleError(
                'No key given.',
                static::RESPONSE_CODE_ERROR_NO_KEY
            );
        }

        return $response;
    }

    /**
     * Create the correct s3 URL for a key
     * @param string $path
     * @return string
     */
    public function generateUrl(string $path): string
    {
        return "{$this->bucketName}/{$path}";
    }

    /**
     * Handle specific errors. Add more here
     * @param exception Normal exception or AWS exceptions
     */
    protected function handleAWSError($exception)
    {
        switch ($exception->getAwsErrorCode()) {
            case 'NoSuchKey':
                $response = $this->handleError(
                    'Key does not exist.',
                    static::RESPONSE_CODE_ERROR_NO_KEY_FOUND,
                    $exception
                );
                break;
            default:
                $response = $this->handleError(
                    'Unhandled error happened.',
                    static::RESPONSE_CODE_ERROR_UNKNOWN,
                    $exception
                );
                break;
        }

        return $response;
    }

    /**
     * Wrap AWS errors for including in response
     * @param $message
     * @param $code
     * @param $exception AWS Exception Object
     * @return array
     */
    protected function handleError($message, $code, $exception = null): array
    {
        $response = parent::handleError($message, $code, $exception);

        if (!empty($exception) && ($exception instanceof S3Exception)) {
            $body = [];
            $body['message'] = $exception->getAwsErrorMessage();
            $body['code'] = $exception->getAwsErrorCode();
            $body['status'] = 'failure';
            $response['body'] = $body;
        }

        return $response;
    }

    /**
     * Fetch the direct download path from S3 & return it as a url
     * @param string $key
     * @param string|null $bucket
     * @return array
     */
    public function getPicUrl(string $key, string $bucket = null)
    {
        //Init response
        $response = $this->initResponse();
        $this->bucketName = !empty($bucket) ? $bucket : $this->bucketName;

        try {
            //Get object
            $exists = $this->client->doesObjectExist($this->bucketName, $key);
            if ($exists) {
                // Construct URL
                $response['body'] = $this->generateUrl($key);
                $response['code'] = static::RESPONSE_CODE_OK;
                $response['message'] = 'Successfully retrieved file.';
            } else {
                // Construct URL
                $response['body'] = null;
                $response['code'] = static::RESPONSE_CODE_ERROR_NO_KEY_FOUND;
                $response['message'] = 'Key does not exist.';
            }
        } catch (AwsException | S3Exception $exception) {
            $response = $this->handleAWSError($exception);
        } catch (Exception $exception) {
            $response = $this->handleError(
                static::RESPONSE_MESSAGE_ERROR_UNHANDLED,
                static::RESPONSE_CODE_ERROR_UNKNOWN,
                $exception
            );
        }

        return $response;
    }

    /**
     * Fetch an image from S3 & return it as an encoded dataUrl
     * @param key name/path of the file
     * @param bucket option bucket override
     */
    public function getPic(string $key, string $bucket = null)
    {
        //Init response
        $response = $this->initResponse();

        try {
            //Get object
            $picData = $this->getObject($key, $bucket);

            //Get body
            $body = $picData['body'];

            //Convert to base64 format
            $base64 = base64_encode($body['data']);

            //Add extra info
            if (!empty($base64)) {
                $base64 = 'data:image/' . $body['object']['ContentType'] . ';base64,' . $base64;
            }

            //Populate response
            $response['body'] = $base64;
            $response['code'] = static::RESPONSE_CODE_OK;
        } catch (AwsException | S3Exception $exception) {
            $response = $this->handleAWSError($exception);
        } catch (Exception $exception) {
            $response = $this->handleError(
                static::RESPONSE_MESSAGE_ERROR_UNHANDLED,
                static::RESPONSE_CODE_ERROR_UNKNOWN,
                $exception
            );
        }

        return $response;
    }

    /**
     * Return a raw object of any kind from S3.
     * @param key name/path of the file
     * @param body string or blob to upload
     * @param bucket option bucket override
     * @return array
     */
    public function getObject(string $key, string $bucket = null)
    {
        //Init response
        $response = $this->initResponse();

        //Check if key has been given
        if (!empty($key)) {
            try {
                $s3Object = $this->client->getObject([
                    'Bucket' => !empty($bucket) ? $bucket : $this->bucketName,
                    'Key' => $key,
                ]);

                // Seek to the beginning of the stream
                $s3Object['Body']->rewind();

                // Read the body off of the underlying stream in chunks
                $fileData = '';
                while ($data = $s3Object['Body']->read(1024)) {
                    $fileData .= $data;
                }

                //Populate response
                $response['body'] = [
                    'object' => $s3Object,
                    'data' => $fileData,
                ];
                $response['code'] = static::RESPONSE_CODE_OK;
            } catch (AwsException | S3Exception $exception) {
                $response = $this->handleAWSError($exception);
            } catch (Exception $exception) {
                $response = $this->handleError(
                    static::RESPONSE_MESSAGE_ERROR_UNHANDLED,
                    static::RESPONSE_CODE_ERROR_UNKNOWN,
                    $exception
                );
            }
        } else {
            $response = $this->handleError(
                'No key given.',
                static::RESPONSE_CODE_ERROR_NO_KEY
            );
        }

        return $response;
    }

    /**
     * Upload a picture passed via dataURL to the given bucket
     * @param key name/path of the file
     * @param encoded dataURL of the image
     * @param bucket option bucket override
     * @return array
     */
    public function putPic(string $key, string $encoded, string $bucket = null)
    {
        //Init response
        $response = $this->initResponse();
        try {
            //Convert from base64 format
            $dataUrl = str_replace(' ', '+', $encoded); // can have spaces added in transport
            $dataUrl = explode(',', $dataUrl);
            $decodedData = base64_decode($dataUrl[1]);

            // get mime type
            $mime = explode(';', $encoded)[0];
            $mime = explode(':', $mime)[1];

            //Populate response
            $s3Object = $this->client->putObject([
                'Bucket' => !empty($bucket) ? $bucket : $this->bucketName, //Required
                'Key' => $key, //Required
                'Body' => $decodedData,
                'Content-Type' => $mime
            ]);
            //Populate response
            $response['body'] = [
                'object' => $s3Object,
                'data' => $this->generateUrl($key),
            ];
            $response['message'] = 'File successfully uploaded';
            $response['code'] = static::RESPONSE_CODE_OK;
        } catch (AwsException | S3Exception $exception) {
            $response['message'] = "Unable to upload file. {$exception->getMessage()}";
            $response = $this->handleAWSError($exception);
        } catch (Exception $exception) {
            $response['message'] = "Unable to upload file. {$exception->getMessage()}";
            $response = $this->handleError(
                static::RESPONSE_MESSAGE_ERROR_UNHANDLED,
                static::RESPONSE_CODE_ERROR_UNKNOWN,
                $exception
            );
        }

        return $response;
    }
}
