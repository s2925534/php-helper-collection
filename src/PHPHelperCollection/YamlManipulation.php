<?php

namespace PHPHelperCollection;

/**
 * Class JsonManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class YamlManipulation extends FileManipulation
{
    /**
     * @var StringManipulation
     */
    private $stringManipulation;

    public function __construct()
    {
        parent::__construct();
        dd(ini_set('yaml_parse', 1));
        $this->stringManipulation = new StringManipulation();
    }

    /**
     * Convert properties to sql case with underscores
     * @param mixed $data
     * @return mixed
     */
    public function sqlCase($data)
    {

        $data = json_decode(json_encode($data), true);
        return array_reduce(
            array_keys($data),
            function ($carry, $key) use ($data) {

                $sqlKey = $this->stringManipulation->lccToSql($key);

                if (!is_array($data[$key])) {
                    $carry[$sqlKey] = $data[$key];
                } else {
                    $carry[$sqlKey] = $this->sqlCase($data[$key]);
                }
                return $carry;
            },
            []
        );
    }

    /**
     * @param string $yaml
     * @return array
     */
    public function transformYamlToArray(string $yaml): array
    {
        // @todo read yaml and cast to array
        $array = yaml_parse($yaml);

        return $array;
    }
}
