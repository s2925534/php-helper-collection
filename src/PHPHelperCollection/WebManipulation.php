<?php

namespace PHPHelperCollection;

/**
 * Class WebManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class WebManipulation extends Helper
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retrives content of web page based on URL given
     * It strips the body content and parse it to a string
     * to be reused
     * @param $url
     * @return false|string
     */
    public function getUrlContent($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($content = file_get_contents($url, true)) {
            curl_close($ch);
        } else {
            exit;
        }

        return $content;
    }

    /**
     * @param $text
     * @param string $tags
     * @param bool $invert
     * @return string|string[]|null
     */
    public function stripTagsContent($text, $tags = '', $invert = false)
    {
        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);
        if (is_array($tags) && count($tags) > 0) {
            if ($invert == false) {
                return preg_replace(
                    '@<(?!(?:' . implode(
                        '|',
                        $tags
                    ) . ')\b)(\w+)\b.*?>.*?</\1>@si',
                    '',
                    $text
                );
            } else {
                return preg_replace(
                    '@<(' . implode(
                        '|',
                        $tags
                    ) . ')\b.*?>.*?</\1>@si',
                    '',
                    $text
                );
            }
        } elseif ($invert == false) {
            return preg_replace(
                '@<(\w+)\b.*?>.*?</\1>@si',
                '',
                $text
            );
        }

        return $text;
    }

    public function checkUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $headers = curl_getinfo($ch);
        curl_close($ch);

        return $headers['http_code'];
    }
}
