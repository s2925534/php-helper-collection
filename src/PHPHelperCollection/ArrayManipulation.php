<?php

namespace PHPHelperCollection;

/**
 * Class ArrayManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class ArrayManipulation extends Helper
{
    /**
     * @var VariableManipulation
     */
    private $VariableManipulation;

    public function __construct()
    {
        parent::__construct();
        $this->VariableManipulation = new VariableManipulation();
    }

    /**
     * Walk through an array of data and recursively convert JSON objects to objects
     * @param array $data
     * @return array
     */
    public function jsonDecode(array $data): array
    {
        $data = json_decode(json_encode($data), true);
        return array_reduce(
            array_keys($data),
            function ($carry, $key) use ($data) {
                if (is_array($data[$key])) {
                    $carry[$key] = $this->jsonDecode($data[$key]);
                } elseif (!is_null($data[$key]) && $this->VariableManipulation->isJson($data[$key])) {
                    $decoded = json_decode($data[$key]);
                    if (is_array($decoded)) {
                        $carry[$key] = $this->jsonDecode($decoded);
                    } else {
                        $carry[$key] = $decoded;
                    }
                } else {
                    $carry[$key] = $data[$key];
                }
                return $carry;
            },
            []
        );
    }


    /**
     * Convert data to lower camel case
     * @param array $data
     * @return array
     */
    public function jsonCase(array $data): array
    {

        $data = json_decode(json_encode($data), true);
        return array_reduce(
            array_keys($data),
            function ($carry, $key) use ($data) {
                if (preg_match("/_/", $key)) {
                    $jsonKey = lcfirst(implode('', array_map(
                        function ($part) {
                            return ucfirst(strtolower($part));
                        },
                        explode('_', $key)
                    )));
                } else {
                    $jsonKey = $key;
                }
                if (!is_array($data[$key])) {
                    $carry[$jsonKey] = $data[$key];
                } else {
                    $carry[$jsonKey] = $this->jsonCase($data[$key]);
                }
                return $carry;
            },
            []
        );
    }

    /**
     * Does a array multiple sort using an array past in via func_get_args()
     * $args array of string arguments
     * @return mixed ideally it will return an sorted array but if fails non array response wil be given
     */
    public function arrSortBy()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row) {
                    $tmp[$key] = $row[$field];
                }
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;

        call_user_func_array('array_multisort', $args);

        return array_pop($args);
    }

    /**
     * Allows the sorting of an array by multiple fields instead of just one.
     * @param array $array
     * @param array $keys
     * @param string $sortBy
     * @return array
     */
    private function sortValuesOfAnArrayByMultipleFields(array $array, array $keys, string $sortBy = null): array
    {
        $sorted = $this->arrSortBy($array, $keys[0], SORT_ASC);
        // Sort by the first two default indexes
        if (!empty($sorted) && !empty($keys[1])) {
            $sorted =
                $this->arrSortBy($array, $keys[0], SORT_ASC, $keys[1], SORT_DESC);
        }
        // Sor by a specific column
        if ($sortBy) {
            $sorted =
                $this->arrSortBy($array, $sortBy, SORT_ASC);
        }

        return $sorted;
    }

    /**
     * Group array items by one of the top level indexes given string
     * @param array $items
     * @param string $groupBy
     * @return array
     */
    public function groupArrayByIndexGivenString(array $items, string $groupBy): array
    {
        $groupedByArray = [];

        foreach ($items as $itemKey => $item) {
            $keys = array_keys($item);
            if ($item[$groupBy]) {
                if (empty($item[$groupBy])) {
                    continue;
                } else {
                    $group = $item[$groupBy];
                }

                $foundAtLeastOne = false;
                foreach ($keys as $arrayKey) {
                    if ($item[$arrayKey]) {
                        $foundAtLeastOne = true;
                        continue;
                    }
                }

                if ($foundAtLeastOne && $groupBy) {
                    if (empty($groupedByArray[$group])) {
                        $groupedByArray[$group] = [];
                    }

                    if ($group) {
                        array_push($groupedByArray[$group], $item);
                    }
                }
            } else {
                continue;
            }
        }

        return $groupedByArray;
    }

    /**
     * @param string $table
     * @param bool $tree
     * @return array
     */
    public function tree(string $table, bool $tree = false): array
    {
        $tableList = $this->getTableList();

        return $tree ? $this->parseTree($tableList) : $tableList;
    }

    /**
     * @param $tree
     * @param null $root
     * @return array|null
     */
    public function parseTree($tree, $root = null)
    {
        $return = array();
        # Traverse the tree and search for direct children of the root
        foreach ($tree as $child => $parent) {
            # A direct child is found
            if ($parent == $root) {
                # Remove item from tree (we don't need to traverse this again)
                unset($tree[$child]);
                # Append the child into result array and parse its children
                $return[] = array(
                    'name' => $child,
                    'children' => $this->parseTree($tree, $child)
                );
            }
        }
        return empty($return) ? null : $return;
    }

    /**
     * @param $tree
     * @return string
     */
    public function printTree($tree): string
    {
        $html = '';
        if (!is_null($tree) && count($tree) > 0) {
            $html .= '<ul>';
            foreach ($tree as $node) {
                $html .= '<li>' . $node['name'];
                $this->printTree($node['children']);
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }

    /**
     * @param $root
     * @param $tree
     * @return string
     */
    public function parseAndPrintTree($root, $tree): string
    {
        $return = array();
        $html = '';
        if (!is_null($tree) && count($tree) > 0) {
            $html .= '<ul>';
            foreach ($tree as $child => $parent) {
                if ($parent == $root) {
                    unset($tree[$child]);
                    $html .= '<li>' . $child;
                    $this->parseAndPrintTree($child, $tree);
                    $html .= '</li>';
                }
            }
            $html .= '</ul>';
        }
        return $html;
    }

    /**
     * @param string $tableName
     * @return array
     */
    private function getTableList(string $tableName = ''): array
    {
        if ($tableName === '') {
            $tableList = array(
                'H' => 'G',
                'F' => 'G',
                'G' => 'D',
                'E' => 'D',
                'A' => 'E',
                'B' => 'C',
                'C' => 'E',
                'D' => null
            );
        } else {
            // @todo get data from database
        }

        return $tableList;
    }
}
