<?php

namespace PHPHelperCollection;

/**
 * Class JsonManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class JsonManipulation extends FileManipulation
{
    /**
     * @var StringManipulation
     */
    private $stringManipulation;

    public function __construct()
    {
        parent::__construct();
        $this->stringManipulation = new StringManipulation();
    }

    /**
     * Convert properties to sql case with underscores
     * @param mixed $data
     * @return mixed
     */
    public function sqlCase($data)
    {

        $data = json_decode(json_encode($data), true);
        return array_reduce(
            array_keys($data),
            function ($carry, $key) use ($data) {

                $sqlKey = $this->stringManipulation->lccToSql($key);

                if (!is_array($data[$key])) {
                    $carry[$sqlKey] = $data[$key];
                } else {
                    $carry[$sqlKey] = $this->sqlCase($data[$key]);
                }
                return $carry;
            },
            []
        );
    }

    /**
     * Transform Json Object to Array
     * @param $request
     * @return array
     */
    private function parseJsonObjectToArray($request): array
    {
        return (json_decode(json_encode($request), true));
    }

    /**
     * Based on path given retrieve all information
     * of a Json file return as an array
     * @param string $fileLocation
     * @return array
     */
    private function getJsonFileContentByLocation(string $fileLocation): array
    {
        $jsonString = file_get_contents($fileLocation);
        if ($jsonString === false) {
            return ['error' => 'Not a valid json file.'];
        }

        $data = json_decode($jsonString, true);
        if ($data === null) {
            return ['error' => 'Unable to parse json file.'];
        }

        return json_decode($jsonString, true);
    }
}
