<?php

namespace PHPHelperCollection;

/**
 * Class FileManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class FileManipulation extends Helper
{
    /**
     * FileManipulation constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $fileName
     * @param $information
     */
    public function writeToFile($fileName, $information)
    {
        $myfile = fopen($fileName, "w") or die("Unable to open file!");
        fwrite($myfile, PHP_EOL . $information);
        fclose($myfile);
    }

    /**
     * @param $url
     * @return string
     */
    public function getBaseNameOfFile($url)
    {
        return basename($url);
    }

    /**
     * @param $fileName
     * @param $list
     */
    public function writeToCSV($fileName, $list)
    {
        $fp = fopen($fileName, 'w');

        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }
}
