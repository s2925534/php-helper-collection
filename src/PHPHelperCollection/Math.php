<?php

namespace PHPHelperCollection;

/**
 * @todo for test sample check https://gist.github.com/ischenkodv/262906
 * Class Math
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class Math extends Helper
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Calculate the MEAN (Average) of a set of numbers in a array
     * @param $array
     * @return float
     */
    public function calculateAverageValuesInAnArray($array): float
    {
        return $array ? (float)array_sum($array) / count($array) : 0;
    }

    /**
     * @param array $array
     * @return float|null
     * @todo verify functionality
     */
    public function medianOfAnArray(array $array)
    {
        if (0 === count($array)) {
            return null;
        }

        $count = count($array);
        asort($array);

        // get the mid-point keys (1 or 2 of them)
        $mid = floor(($count - 1) / 2);
        $keys = array_slice(array_keys($array), $mid, (1 === $count % 2 ? 1 : 2));
        $sum = 0;
        foreach ($keys as $key) {
            $sum += $array[$key];
        }

        return (float)($sum / count($keys));
    }

    /**
     * @param array $array
     * @return float|null
     * @todo
     * Verify the MODE osf an array
     */
    public function modeOfAnArray(array $array)
    {
        if (0 === count($array)) {
            return null;
        }

        return $mode;
    }
}
