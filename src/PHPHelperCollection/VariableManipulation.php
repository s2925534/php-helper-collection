<?php

namespace PHPHelperCollection;

/**
 * Holds helper functions to be used across the system
 * Class VariableManipulation
 * @package App\Helpers
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class VariableManipulation extends Helper
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Verify is a variable is truly contains a json object
     * @param $string
     * @return bool
     */
    public static function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Identify if a input given is truly an integer
     * @param $input
     * @return bool
     */
    public function isInteger($input)
    {
        return (ctype_digit(strval($input)));
    }
}
