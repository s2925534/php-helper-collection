<?php

namespace PHPHelperCollection;

/**
 * Class MainHelper
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class Helper
{
    /**
     * @var array
     */
    protected $helperResponse;

    /**
     * MainHelper constructor.
     */
    public function __construct()
    {
        $this->initResponse();
    }

    /**
     * @return void
     */
    protected function initResponse()
    {
        $this->helperResponse = [
            'status' => '',
            'message' => '',
            'data' => [],
            'code' => null,
        ];
    }

    /**
     * @param array $something
     * @return array
     */
    public function sample(array $something = []): array
    {
        return $something;
    }
}
