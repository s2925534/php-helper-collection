<?php

namespace PHPHelperCollection;

/**
 * Class ImageManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class URLManipulation extends Helper
{
    /**
     * ImageManipulation constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $url
     * @return int
     */
    public function checkUrl(string $url, string $return = 'code'): int
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $headers = curl_getinfo($ch);
        curl_close($ch);

        if ($return === 'code') {
            return (int)$headers['http_code'];
        }

        return $data;
    }
}
