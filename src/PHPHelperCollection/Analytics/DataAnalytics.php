<?php

namespace PHPHelperCollection\Analytics;

use PHPHelperCollection\Helper;

/**
 * @todo consider creating a separated library for Data Analytics that includes this library as helper
 * Class DataAnalytics
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class DataAnalytics extends Helper
{
    /**
     * DataAnalitycs constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Return a assosiative array with the bag of words
     * for given files' locations
     * @param array $documentsArray
     * @return array
     */
    public function getBagOfWordsByGivenFiles(array $documentsArray): array
    {
        // Open documents from physical location
        $documentsArrayOfStrings = $this->convertDocumentToString($documentsArray);

        // @todo consider using the php function str_word_count to count the words for a TF/IDF

        // Retrieve bag of words based
        $documentsArray = $this->getBagOfWordsByGivenString();

        return $documentsArray;
    }

    /**
     * Converts a collection of textual files
     * into a collection of textual strings
     * performs data cleaning
     * @param array $documentsArray
     * @return array
     */
    private function convertDocumentToString(array $documentsArray)
    {
        $documentsStrings = [];

        foreach ($documentsArray as $key => $item) {
            $dKey = $key + 1;
            $documentsStrings["d$dKey"] = $this->getFileContent($item);
            $documentsStrings["d$dKey"] = $this->getFileContentProcessed($documentsStrings["d$key"]);
        }

        return $documentsStrings;
    }

    /**
     * Return file content as a single string
     * verify for special characters
     * remove stop words
     * remove punctuation
     * @param $documentContent
     * @param string|null $documentType
     * @return string
     */
    private function getFileContent($documentContent, string $documentType = null): string
    {
        // If document type is null
        // try to find document type of file
        if (!$documentType) {
            // @todo identify file type
            // @todo maybe in a switch statements of supported types of file
            $documentContent = 'DO SOMETHING AROUND HERE';
        } else {
            $documentContent = file_get_contents($documentContent);
        }

        // text file will be the default type
        // no verification necessary
        // If no type is specified try and teat as regular text file directly
        return $documentContent;
    }

    /**
     * @param string $documentContent
     */
    private function getFileContentProcessed(string $documentContent)
    {

        // @todo lower case all the content
        $documentContent = strtolower($documentContent);

        // @todo remove stop words
        $documentContent = $this->removeStopWords($documentContent);

        // @todo remove unimportant words such as boy, girl, he, she
        $documentContent = $this->removeNonImportantWords($documentContent);

        // @todo remove accents
        $documentContent = $this->removeAccents($documentContent);

        // @todo remove punctuation
        $documentContent = $this->removePunctuations($documentContent);


        return $documentContent;
    }

    /**
     * @param string $documentContent
     * @return string
     */
    private function removeStopWords(string $documentContent): string
    {
        // @todo regex
        return $documentContent;
    }

    /**
     * @param string $documentContent
     * @return string
     */
    private function removeNonImportantWords(string $documentContent): string
    {
        // @todo check against dictionary. maybe an API?
        return $documentContent;
    }

    /**
     * @param string $documentContent
     * @return string
     */
    private function removeAccents(string $documentContent): string
    {
        // @todo check geo functions tha replaces other languages characters to english characters
        return $documentContent;
    }

    /**
     * @param string $documentContent
     * @return string
     */
    private function removePunctuations(string $documentContent): string
    {
        // @todo regex
        return $documentContent;
    }

    /**
     * Return a assosiative array with the bag of words
     * for given collection of strings
     * @param array $stringsArray
     * @return array
     */
    public function getBagOfWordsByGivenString(array $stringsArray): array
    {
        return $stringsArray;
    }

    /**
     * @param array $dataset
     * @return array
     */
    public function getMediumOfDataSet(array $dataset): array
    {
        // example array(1,3,10,14,5,18,20)
        // return the array of median if an in multiple rows
        foreach ($dataset as $key => $set) {
            $dataset[$key] = $this->getMediumOfDatasetRow($set);
        }

        return $dataset;
    }

    /**
     * @param $set
     * @return float
     */
    private function getMediumOfDatasetRow($set): float
    {
        // @todo calculate the median of a set of values
        $calculatedValue = '<SOME CALCULATION HERE>';

        return $calculatedValue;
    }
}
