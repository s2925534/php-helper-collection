<?php

namespace PHPHelperCollection;

use Exception;
use PDO;

/**
 * Class DataManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class DataManipulation extends Helper
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Collects PDO object based on credentials and host information given
     * @param $host
     * @param $name
     * @param $username
     * @param $password
     */
    public function getPDO($host, $name, $username, $password)
    {
        $pdo = new PDO("mysql:host=$host;dbname=$name", $username, $password);
    }

    private function strinpDBError(Exception $exception)
    {
        $messageArray = [];
        $clean = substr($exception->getMessage(), 0, strrpos($exception->getMessage(), '_')) . "'";

        preg_match_all('/\'.*?\'|\'.*?\'/', $clean, $messageArray);
        $field = ucwords(strtoupper((str_replace('_', ' ', str_replace('IDX', '', $messageArray[0][1])))));

        return "The value " . $messageArray[0][0] . " given for $field is not unique. Try a different one.";
    }
}
