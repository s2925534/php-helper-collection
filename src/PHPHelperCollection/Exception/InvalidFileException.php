<?php

namespace PHPHelperCollection\Exception;

use Exception;
use Throwable;

/**
 * Class InvalidFileException
 * @package App\Exception
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class InvalidFileException extends Exception
{
    /**
     * @var Error message for exception
     */
    protected $message;

    /**
     * InvalidFileException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Invalid File Given.", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Set custom message.
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Set custom code
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
