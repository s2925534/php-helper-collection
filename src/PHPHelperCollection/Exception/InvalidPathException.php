<?php

namespace PHPHelperCollection\Exception;

use Exception;
use Throwable;

/**
 * Class InvalidPathException
 * @package App\Exception
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class InvalidPathException extends Exception
{
    /**
     * @var Error message for exception
     */
    protected $message;

    /**
     * InvalidPathException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Invalid path given.", $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Set customm message
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Set custom code
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
