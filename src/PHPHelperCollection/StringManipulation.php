<?php

namespace PHPHelperCollection;

/**
 * Class StringManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class StringManipulation extends Helper
{
    protected $regexLibrary;

    public function __construct()
    {
        parent::__construct();
        $this->regexLibrary = new RegexLibrary();
    }

    /**
     * Convert a lowerCamelCase string to an sql_string_style string
     * @param string $string
     * @return string
     */
    public function lccToSql(string $string): string
    {
        $split = preg_split(
            "/([A-Z])/",
            $string,
            -1,
            PREG_SPLIT_DELIM_CAPTURE
        );

        $sqlCase = array_reduce(
            array_keys($split),
            function ($carry, $key) use ($split) {
                if (($key % 2)) {
                    $carry .= '_' . strtolower($split[$key]);
                } else {
                    $carry .= $split[$key];
                }

                return $carry;
            },
            ''
        );

        return $sqlCase;
    }

    /**
     * Converts string with a char specified for replacement by another char
     * and set string to lowerCamelCase
     * i.e.: desktop-computer-aparatues
     * specify - (dash) to be replaced by _ and the results will be
     * desktop
     * @param $string
     * @param string $from
     * @param string $to
     * @return string
     */
    public function replaceCharByCharAndLowerCamelCase($string, $from = '-', $to = '_'): string
    {
        $i = array($from, $to);
        $string = preg_replace('/([a-z])([A-Z])/', "\\1 \\2", $string);
        $string = preg_replace('@[^a-zA-Z0-9\-_ ]+@', '', $string);
        $string = str_replace($i, ' ', $string);
        $string = str_replace(' ', '', ucwords(strtolower($string)));

        return strtolower(substr($string, 0, 1)) . substr($string, 1);
    }

    /**
     * Transform Camel Case string
     * @param $str
     * @return string
     */
    public function unCamelCase($str)
    {
        $str = preg_replace('/([a-z])([A-Z])/', "\\1_\\2", $str);

        return strtolower($str);
    }

    /**
     * Transform Class Name Table to class name
     * @param string $className
     * @return string
     */
    public function classNameToTable(string $className): string
    {
        $classParts = explode('\\', $className);
        $className = array_pop($classParts);
        $split = array_values(array_filter(
            preg_split("/([A-Z])/", $className, -1, PREG_SPLIT_DELIM_CAPTURE)
        ));

        return array_reduce(
            array_keys($split),
            function ($carry, $key) use ($split) {
                if (!($key % 2)) {
                    $carry .= (($key) ? '_' : '') . strtolower($split[$key]);
                } else {
                    $carry .= $split[$key];
                }

                return $carry;
            },
            ''
        );
    }


    /**
     * Convert string given to english characters. Remove accents, symbols, etc.
     * @param $str
     * @return string|string[]
     */
    public function transformStringToEnglishCharacters($str)
    {
        $search = explode(
            ",",
            "ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ"
        );
        $replace = explode(
            ",",
            "c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,
            O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE"
        );

        return str_replace($search, $replace, $str);
    }

    /**
     * @param $field
     * @param string $toCase
     * @return mixed
     */
    public function transformStringToCase($field, $toCase = 'lower')
    {
        if (!empty($field) && $toCase == 'lower') {
            $field = strtolower($field);
        } elseif (!empty($field) && $toCase == 'upper') {
            $field = strtoupper($field);
        } else {
            $field = null;
        }
        return $field;
    }

    /**
     * Identify if a input given is truly an integer
     * @param $input
     * @return bool
     */
    public function isInteger($input)
    {
        return (ctype_digit(strval($input)));
    }
}
