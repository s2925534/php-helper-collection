<?php

namespace PHPHelperCollection;

/**
 * Class StringManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class RegexLibrary extends Helper
{
    /**
     * Positive lookahead ensuring that at least one numeric character is present.
     * @var string
     */
    protected $lookaheadNumeric = '(?=.*?\p{N})';

    /**
     * Positive lookahead ensuring that at least one symbol, punctuation, or space is used.
     * @var string
     */
    protected $lookaheadSymbol = '(?=.*?[\p{S}\p{P} ]';

    /**
     * Positive lookahead ensuring that at least one uppercase letter is used.
     * @var string
     */
    protected $lookaheadUpper = '(?=.*?\p{Lu})';

    /**
     * Positive lookahead ensuring that at least one lowercase letter is used.
     * @var string
     */
    protected $lookaheadLower = '(?=.*?\p{Ll})';

    /**
     * Match any non-control character between 8 and 16 times.
     * @var string
     */
    protected $lookaheadNonControl;

    /**
     * To verify if at least one numeric character is present.
     * @var string
     */
    protected $numeric = '[\d!$%^&]';

    /**
     * To verify if at least one UPPERCASE letter is used.
     * @var string
     */
    protected $upper = '[\d!$%^&]';

    /**
     * To verify if at least one lowercase letter is used.
     * @var string
     */
    protected $lower = '[\d!$%^&]';

    /**
     * To verify if at least CAPITAL alpha characters is used.
     * @var string
     */
    private $lookaheadAlphaUpper = '([A-Z])';

    /**
     * To verify if at least CAPITAL alpha characters is used.
     * @var string
     */
    private $lookaheadAlphaLower = '([a-z])';

    /**
     * @todo check for symbol characters only
     * @var string
     */
    private $symbol = '';

    /**
     * @todo check for alpha and capital characters only
     * @var string
     */
    private $alphaUpper = '[A-Z]';

    /**
     * @todo check for alpha and capital characters only
     * @var string
     */
    private $alphaLower = '[a-z]';

    public function __construct()
    {
        parent::__construct();
    }

    public function getRegexNonControl(int $min = 8, int $max = 16)
    {
        return "[^\p{C}]{" . $min . "," . $max . "}";
    }

    /**
     * Ensuring that at least one numeric character is present.
     * @param bool $lookahead
     * @return string
     */
    public function getRegexForNumeric(bool $lookahead = false): string
    {
        return $lookahead ? $this->lookaheadNumeric : $this->numeric;
    }

    /**
     * Ensuring that at least one UPPERCASE character is present.
     * @param bool $lookahead
     * @return string
     */
    public function getRegexForUpper(bool $lookahead = false): string
    {
        return $lookahead ? $this->lookaheadUpper : $this->upper;
    }

    /**
     * Ensuring that at least one lowercase character is present.
     * @param bool $lookahead
     * @return string
     */
    public function getRegexForLower(bool $lookahead = false): string
    {
        return $lookahead ? $this->lookaheadLower : $this->lower;
    }

    /**
     * Ensuring that at least one symbol character is present.
     * @param bool $lookahead
     * @return string
     */
    public function getRegexForSymbol(bool $lookahead = true): string
    {
        return $lookahead ? $this->lookaheadSymbol : $this->symbol;
    }

    /**
     * @param bool $lookahead
     * @return string
     */
    public function getRegexForAlphaUpper(bool $lookahead = true): string
    {
        return $lookahead ? $this->lookaheadAlphaUpper : $this->alphaUpper;
    }

    /**
     * @param bool $lookahead
     * @return string
     */
    public function getRegexForAlphaLower(bool $lookahead = true): string
    {
        return $lookahead ? $this->lookaheadAlphaLower : $this->alphaLower;
    }
}
