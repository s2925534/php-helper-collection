<?php

namespace PHPHelperCollection;

/**
 * Class Password
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class Password extends StringManipulation
{
    /** VALIDATION MESSAGES **/
    private const PASSWORD_MESSAGE_BLANK = "Password cannot be blank";
    private const PASSWORD_MESSAGE_NO_MATCH = "Passwords don't match";
    private const PASSWORD_MESSAGE_ALREADY_USED = "Cannot use the same password twice";
    private const PASSWORD_MESSAGE_CONTAINS_USER_NAME = "You cannot use your name in your password";
    private const PASSWORD_MESSAGE_SIMILAR_TO_PREVIOUS = "New password is too similar to the old one";
    private const PASSWORD_MESSAGE_GOOD_PASSWORD = "Password is compliant";
    private const PASSWORD_MESSAGE_GOOD_PASSWORD_STRENGTH = "Password is compliant";
    private const PASSWORD_MESSAGE_BAD_PASSWORD = "Bad password! Try again.";
    private const PASSWORD_MESSAGE_UNKNOWN = "Unknown error. Try again.";

    /**
     * @var RegexLibrary
     */
    protected $regexLibrary;

    /**
     * Password constructor.
     */
    public function __construct()
    {
        $this->regexLibrary = new RegexLibrary();
        parent::__construct();
    }

    /**
     * Checks the provided current user password and new password against Aerocare's password requirements.
     * @param string $currentPassword The current password of the user
     * @param string $password The password to be set for the user
     * @param string $passwordConfirmation The password to be set for the user again to ensure no typing errors
     * @param array $userInformation
     * @return array
     */
    public function checkPasswordRequirements(
        string $currentPassword,
        string $password,
        string $passwordConfirmation,
        $userInformation = []
    ): array {
        $result = null;

        if (!$password || trim($password) == '') {
            $result = [
                'is_valid' => false,
                'message' => self::PASSWORD_MESSAGE_BLANK
            ];
        } elseif ($password != $passwordConfirmation) {
            $result = [
                'is_valid' => false,
                'message' => self::PASSWORD_MESSAGE_NO_MATCH
            ];
        } elseif ($password == $currentPassword) {
            $result = [
                'is_valid' => false,
                'message' => self::PASSWORD_MESSAGE_ALREADY_USED
            ];
        } elseif (!empty($userInformation) &&
            strpos(strtolower($password), strtolower($userInformation['first_name'])) !== false) {
            $result = ['is_valid' => false, 'message' => self::PASSWORD_MESSAGE_CONTAINS_USER_NAME];
        } elseif (strpos(strtolower($password), strtolower($userInformation['surname'])) !== false) {
            $result = [
                'is_valid' => false,
                'message' => self::PASSWORD_MESSAGE_CONTAINS_USER_NAME
            ];
        } elseif (strtolower($password) == strtolower($currentPassword)) {
            $result = [
                'is_valid' => false,
                'message' => self::PASSWORD_MESSAGE_SIMILAR_TO_PREVIOUS
            ];
        } elseif ($check = $this->checkPasswordStrength($password)) {
            if (!empty($check) && $check['isValid'] === true) {
                return [
                    'is_valid' => false,
                    'message' => self::PASSWORD_MESSAGE_GOOD_PASSWORD_STRENGTH . $check['message']
                ];
            }
        }
        if (!$result) {
            return ['is_valid' => false, 'message' => self::PASSWORD_MESSAGE_BAD_PASSWORD];
        } else {
            return ['is_valid' => false, 'message' => self::PASSWORD_MESSAGE_UNKNOWN];
        }
    }

    /**
     * Checks the strength of a given password.
     * @param string $password the plain text representation of the password to be checked
     * @return array
     */
    private function checkPasswordStrength($password)
    {
        $msg = '';

        // Password must be at least 8 characters long
        if (strlen($password) < 8) {
            $msg = "Password must be a minimum of 8 characters long";
        }
        // Password unique characters must make up half or more of the password
        if (strlen(count_chars($password, 3)) < (strlen($password) / 2)) {
            $msg = "Password is too simple. Please use more unique characters";
        }
        // Password must contain at least one uppercase, lowercase and numeric character
        if (!preg_match('/' . $this->regexLibrary->getRegexForAlphaUpper() . '+/', $password)) {
            $msg = "Password must contain at least one uppercase character";
        }
        if (!preg_match('/' . $this->regexLibrary->getRegexForAlphaLower() . '+/', $password)) {
            $msg = "Password must contain at least one lowercase character";
        }
        if (!preg_match('/' . $this->regexLibrary->getRegexForNumeric() . '+/', $password)) {
            $msg = "Password must contain at least one number";
        }

        if ($msg !== '') {
            return ['isValid' => false, 'message' => $msg];
        }

        return ['isValid' => true, 'message' => self::PASSWORD_MESSAGE_GOOD_PASSWORD];
    }
}
