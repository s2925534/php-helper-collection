<?php

namespace PHPHelperCollection;

/**
 * Class ImageManipulation
 * @package PHPHelperCollection
 * @author Pedro Veloso <it.specialist@hotmail.com>
 */
class ImageManipulation extends FileManipulation
{
    /**
     * ImageManipulation constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $sourceImagePath
     * @param $thumbnailImagePath
     * @param int $thumbnailWith
     * @param int $thumbnailHeight
     * @return bool
     */
    public static function generateImageThumbnail(
        $sourceImagePath,
        $thumbnailImagePath,
        $thumbnailWith = 250,
        $thumbnailHeight = 300
    ) {
        list(
            $sourceImageWidth,
            $sourceImageHeight,
            $source_image_type
            ) = getimagesize($sourceImagePath);
        switch ($source_image_type) {
            case IMAGETYPE_GIF:
                $sourceGdImage = imagecreatefromgif($sourceImagePath);
                break;
            case IMAGETYPE_JPEG:
                $sourceGdImage = imagecreatefromjpeg($sourceImagePath);
                break;
            case IMAGETYPE_PNG:
                $sourceGdImage = imagecreatefrompng($sourceImagePath);
                break;
        }
        if ($sourceGdImage === false) {
            return false;
        }
        $sourceAspectRatio = $sourceImageWidth / $sourceImageHeight;
        $thumbnailAspectRatio = $thumbnailWith / $thumbnailHeight;
        if ($sourceImageWidth <= $thumbnailWith &&
            $sourceImageHeight <= $thumbnailHeight
        ) {
            $thumbnailImageWidth = $sourceImageWidth;
            $thumbnailImageHeight = $sourceImageHeight;
        } elseif ($thumbnailAspectRatio > $sourceAspectRatio
        ) {
            $thumbnailImageWidth = (int)($thumbnailWith * $sourceAspectRatio);
            $thumbnailImageHeight = $thumbnailHeight;
        } else {
            $thumbnailImageWidth = $thumbnailWith;
            $thumbnailImageHeight = (int)($thumbnailHeight / $sourceAspectRatio);
        }
        $thumbnail_gd_image = imagecreatetruecolor(
            $thumbnailImageWidth,
            $thumbnailImageHeight
        );
        imagecopyresampled(
            $thumbnail_gd_image,
            $sourceGdImage,
            0,
            0,
            0,
            0,
            $thumbnailImageWidth,
            $thumbnailImageHeight,
            $sourceImageWidth,
            $sourceImageHeight
        );
        imagejpeg(
            $thumbnail_gd_image,
            $thumbnailImagePath,
            95
        );
        imagedestroy($sourceGdImage);
        imagedestroy($thumbnail_gd_image);
        return true;
    }

    /**
     * Validate base64 string
     * @param $data
     * @return bool
     */
    private function validateBase64($data): bool
    {
        // Done this way to ensure that after encoding and
        // decoding the validity of the image will not differ
        return (base64_encode(base64_decode($data, true)) === $data);
    }

    /**
     * @param $file
     * @return mixed|string
     */
    private function extractInformationFromFullBase64String($file): array
    {
        $file = explode(';', $file);
        $content['type'] = explode('/', $file[0])[1];
        $content['data'] = explode(',', $file[1])[1];

        return $content;
    }
}
